package com.assignment.Assignment2Java.dataaccess.model;

public class CustomerSpender {

    Customer customer;
    double total;

    public CustomerSpender(Customer customer, double total) {
        this.customer = customer;
        this.total = total;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
