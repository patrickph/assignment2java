package com.assignment.Assignment2Java.dataaccess.model;

import java.util.ArrayList;

public class CustomerGenre {

    String customerId;
    ArrayList<String> genre;

    public CustomerGenre(String customerId, ArrayList<String> genre) {
        this.customerId = customerId;
        this.genre = genre;
    }

    public String getCustomer() {
        return customerId;
    }

    public void setCustomer(String customerId) {
        this.customerId = customerId;
    }

    public ArrayList<String> getGenre() {
        return genre;
    }

    public void setGenre(ArrayList<String> genre) {
        this.genre = genre;
    }
}
