package com.assignment.Assignment2Java.dataaccess.model;

public class Customer {

    private String id;
    private String firstName;
    private String lastName;
    private String country;
    private String postalCode;
    private String phone;
    private String email;

    public Customer(String id, String firstName, String lastName, String country, String postalCode, String phone, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String getCustomers = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
    String getCustomerById = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId =" + id;
    String getCustomerByName = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE " + firstName + " AND LastName like " + lastName;
    String pageOfCustomers = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer  + limit +  OFFSET  + offset";
    String insertCustomer = "INSERT INTO Customer ( FirstName, LastName, Country, PostalCode, Phone, Email) VALUES " + "VALUESHERE";
    String updateCustomer = "UPDATE Customer SET FirstName = value, LastName = value, Country = value, PostalCode = value, Phone = value, Email = value WHERE CustomerId = " + id;
    String countryCount = "SELECT count(), Country from Customer GROUP BY Country ORDER BY count() DESC";
    String highestSpenders = "SELECT FirstName, LastName, Total from Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId ORDER BY Total DESC";
    String getMostPopularGenre = "SELECT Name, Popularity FROM (SELECT G.Name, count(G.Name) as Popularity FROM Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId INNER JOIN Track T on T.TrackId = IL.TrackId INNER JOIN Genre G on G.GenreId = T.GenreId WHERE I.CustomerId = ? GROUP BY G.Name) WHERE Popularity IN (SELECT max(Popularity) FROM (SELECT G.Name, count(G.Name) as Popularity FROM Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId INNER JOIN Track T on T.TrackId = IL.TrackId INNER JOIN Genre G on G.GenreId = T.GenreId WHERE I.CustomerId = ? GROUP BY G.Name))";
}
