package com.assignment.Assignment2Java.dataaccess.model;

public class CustomerCountry {

    private String numCustomers;
    private String name;

    public CustomerCountry(String numCustomers, String name) {
        this.numCustomers = numCustomers;
        this.name = name;
    }

    public String getNumCustomers() {
        return numCustomers;
    }

    public void setNumCustomers(String numCustomers) {
        this.numCustomers = numCustomers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
