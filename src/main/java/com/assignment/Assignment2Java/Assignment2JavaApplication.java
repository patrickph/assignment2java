package com.assignment.Assignment2Java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2JavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment2JavaApplication.class, args);
	}

}
