package com.assignment.Assignment2Java.controller;

import com.assignment.Assignment2Java.dataaccess.model.Track;
import com.assignment.Assignment2Java.repository.TrackRepository;
import com.assignment.Assignment2Java.repository.TrackRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.ServerEndpoint;
import java.util.ArrayList;

@Controller
public class TrackController {

    @Autowired
    TrackRepository trep;

    // Endpoints

    @GetMapping("/")
    public String home(Model model) {
        // Adds data from the repository and exposes to the HTML
        model.addAttribute("tracks", trep.getRandomTracks());
        model.addAttribute("artists", trep.getRandomArtists());
        model.addAttribute("genres", trep.getRandomGenres());
        return "home";
    }

    @GetMapping("/search")
    public String search(@RequestParam (value = "term") String track, Model model) {
        // Adds data from the repository and exposes to the HTML
        model.addAttribute("trackInfo", trep.getTrackInfo(track));
        return "search";
    }
}
