package com.assignment.Assignment2Java.controller;

import com.assignment.Assignment2Java.dataaccess.model.Customer;
import com.assignment.Assignment2Java.dataaccess.model.CustomerCountry;
import com.assignment.Assignment2Java.dataaccess.model.CustomerGenre;
import com.assignment.Assignment2Java.dataaccess.model.CustomerSpender;
import com.assignment.Assignment2Java.repository.CustomerRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    @Autowired
    CustomerRepositoryImpl repo;

    // Endpoints

    @GetMapping("/api/customers")
    public ArrayList<Customer> getAllCustomers() {
        return repo.getAllCustomers();
    }

    @GetMapping("/api/customers/{id}")
    public Customer getCustomerById(@PathVariable String id) {
        return repo.getCustomerById(id);
    }

    @GetMapping("/api/customer/{name}")
    public Customer getCustomerByName(@PathVariable String name) {
        return repo.getCustomerByName(name);
    }

    @GetMapping("/api/customers/{limit}/{offset}")
    public ArrayList<Customer> getPageOfCustomers(@PathVariable String limit, @PathVariable String offset) {
        return repo.getPageOfCustomers(limit, offset);
    }

    @PostMapping("/api/customers")
    public boolean addCustomer(@RequestBody Customer customer) {
        //Not tested
        return repo.addCustomer(customer);
    }

    @PatchMapping("/api/customers")
    public boolean updateCustomer(@RequestBody Customer customer) {
        //Not tested
        return repo.updateCustomer(customer);
    }

    @GetMapping("/api/customers/by/country")
    public ArrayList<CustomerCountry> getNumberOfCustomersByCountry() {
        return repo.getNumberOfCustomersByCountry();
    }

    @GetMapping("/api/spenders")
    public ArrayList<CustomerSpender> getSpenders() {
        return repo.getSpenders();
    }

    @GetMapping("/api/customers/{id}/popular/genre")
    public CustomerGenre getCustomerPopularGenre(@PathVariable String id) {
        return repo.getCustomerGenre(id);
    }

}
