package com.assignment.Assignment2Java.repository;

import com.assignment.Assignment2Java.dataaccess.model.Track;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

@Repository
public class TrackRepositoryImpl implements TrackRepository{

    // Connection URL to the DB
    private static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // SQL Queries
    private final String getAllTracks = "SELECT Name FROM Track";
    private final String getAllArtists = "SELECT Name FROM Artist";
    private final String getAllGenres = "SELECT Name FROM Genre";
    private final String getTrackInfo = "SELECT Track.Name as Name, A2.Name as Artist, A.Title as Album, G.Name as Genre FROM Track INNER JOIN Album A on A.AlbumId = Track.AlbumId INNER JOIN Genre G on G.GenreId = Track.GenreId INNER JOIN Artist A2 on A2.ArtistId = A.ArtistId WHERE Track.Name LIKE ?";
    private final String getTrackInfoTest = "SELECT Track.Name as Name, A2.Name as Artist, A.Title as Album, G.Name as Genre FROM Track INNER JOIN Album A on A.AlbumId = Track.AlbumId INNER JOIN Genre G on G.GenreId = Track.GenreId INNER JOIN Artist A2 on A2.ArtistId = A.ArtistId WHERE Track.Name LIKE '%a%'";

    /**
     * Method for fetching and returning five random tracks from the database
     * @return ArrayList of five random tracks
     */
    @Override
    public ArrayList<String> getRandomTracks() {
        ArrayList<String> tracks = new ArrayList<>();
        ArrayList<String> allTracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement stmt = conn.prepareStatement(getAllTracks);
            ResultSet set = stmt.executeQuery();
            while(set.next()) {
                allTracks.add(set.getString("Name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }

            Random rn = new Random();
            for (int i = 0; i < 5; i++) {
                tracks.add(allTracks.get(rn.nextInt(allTracks.size())));
            }
        }
        return tracks;
    }

    /**
     * Method for fetching and returning five random artists from the database
     * @return ArrayList of five random artists
     */
    @Override
    public ArrayList<String> getRandomArtists() {
        ArrayList<String> artists = new ArrayList<>();
        ArrayList<String> allArtists = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement stmt = conn.prepareStatement(getAllArtists);
            ResultSet set = stmt.executeQuery();
            while(set.next()) {
                allArtists.add(set.getString("Name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }

            Random rn = new Random();
            for (int i = 0; i < 5; i++) {
                artists.add(allArtists.get(rn.nextInt(allArtists.size())));
            }
        }
        return artists;
    }

    /**
     * Method for fetching and returning five random genres from the database
     * @return ArrayList of five random genres
     */
    @Override
    public ArrayList<String> getRandomGenres() {
        ArrayList<String> genres = new ArrayList<>();
        ArrayList<String> allGenres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement stmt = conn.prepareStatement(getAllGenres);
            ResultSet set = stmt.executeQuery();
            while(set.next()) {
                allGenres.add(set.getString("Name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }

            Random rn = new Random();
            for (int i = 0; i < 5; i++) {
                genres.add(allGenres.get(rn.nextInt(allGenres.size())));
            }
        }
        return genres;
    }

    /**
     * Method for finding all tracks containing a search term
     * @param track - search term
     * @return ArrayList of all matching tracks
     */
    @Override
    public ArrayList<Track> getTrackInfo(String track) {
        ArrayList<Track> tracks = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement stmt = conn.prepareStatement(getTrackInfo);
            stmt.setString(1,  "%" + track + "%");
            ResultSet set = stmt.executeQuery();
            while(set.next()) {
                tracks.add( new Track(
                        set.getString("Name"),
                        set.getString("Artist"),
                        set.getString("Album"),
                        set.getString("Genre")
                ));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;
    }
}
