package com.assignment.Assignment2Java.repository;

import com.assignment.Assignment2Java.dataaccess.model.Customer;
import com.assignment.Assignment2Java.dataaccess.model.CustomerCountry;
import com.assignment.Assignment2Java.dataaccess.model.CustomerGenre;
import com.assignment.Assignment2Java.dataaccess.model.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    // Connection URL to the DB
    private static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    // SQL Queries
    private final String getCustomers = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
    private final String getCustomerById = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId =?";
    private final String getCustomerByName = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE ? AND LastName like ?";
    private final String getPageOfCustomers = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ?  OFFSET  ?";
    private final String insertCustomer = "INSERT INTO Customer ( FirstName, LastName, Country, PostalCode, Phone, Email ) VALUES (?, ?, ?, ?, ?, ?)";
    private final String updateCustomer = "UPDATE Customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?";
    private final String countryCount = "SELECT count() as Count, Country from Customer GROUP BY Country ORDER BY count() DESC";
    private final String highestSpenders = "SELECT Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, Total from Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId ORDER BY Total DESC";
    private final String getMostPopularGenre = "SELECT Name, Popularity FROM (SELECT G.Name, count(G.Name) as Popularity FROM Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId INNER JOIN Track T on T.TrackId = IL.TrackId INNER JOIN Genre G on G.GenreId = T.GenreId WHERE I.CustomerId = ? GROUP BY G.Name) WHERE Popularity IN (SELECT max(Popularity) FROM (SELECT G.Name, count(G.Name) as Popularity FROM Customer INNER JOIN Invoice I on Customer.CustomerId = I.CustomerId INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId INNER JOIN Track T on T.TrackId = IL.TrackId INNER JOIN Genre G on G.GenreId = T.GenreId WHERE I.CustomerId = ? GROUP BY G.Name))";

    private Connection conn = null;

    /**
     * Method for getting all customers from the database
     * @return ArrayList of Customer objects
     */
    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(getCustomers);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                customers.add(
                        new Customer(
                                res.getString("CustomerId"),
                                res.getString("FirstName"),
                                res.getString("LastName"),
                                res.getString("Country"),
                                res.getString("PostalCode"),
                                res.getString("Phone"),
                                res.getString("Email")
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    /**
     * Method for getting a specific customer based on ID
     * @param id - ID of the requested customer
     * @return Customer object of the requested customer
     */
    @Override
    public Customer getCustomerById(String id) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(getCustomerById);
            prep.setString(1, id);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                customer =
                    new Customer(
                            res.getString("CustomerId"),
                            res.getString("FirstName"),
                            res.getString("LastName"),
                            res.getString("Country"),
                            res.getString("PostalCode"),
                            res.getString("Phone"),
                            res.getString("Email")
                    );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return customer;
    }

    /**
     * Method for getting a specific customer based on Name
     * @param name - Name of the requested customer
     * @return Customer object of the requested customer
     */
    @Override
    public Customer getCustomerByName(String name) {
        Customer customer = null;
        String[] names = name.split("_");
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(getCustomerByName);
            prep.setString(1, names[0]);
            prep.setString(2, names[1]);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                customer =
                        new Customer(
                                res.getString("CustomerId"),
                                res.getString("FirstName"),
                                res.getString("LastName"),
                                res.getString("Country"),
                                res.getString("PostalCode"),
                                res.getString("Phone"),
                                res.getString("Email")
                        );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return customer;
    }

    /**
     * Method for getting a subset of the customers in the database
     * @param limit - Number of Customers to be returnes
     * @param offset - Offset for where to start in the database
     * @return ArrayList of the requested customers
     */
    @Override
    public ArrayList<Customer> getPageOfCustomers(String limit, String offset) {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(getPageOfCustomers);
            prep.setString(1, limit);
            prep.setString(2, offset);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                customers.add(
                        new Customer(
                                res.getString("CustomerId"),
                                res.getString("FirstName"),
                                res.getString("LastName"),
                                res.getString("Country"),
                                res.getString("PostalCode"),
                                res.getString("Phone"),
                                res.getString("Email")
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    /**
     * Method for posting a Customer to the database
     * @param customer - Customer object to be added to the database
     * @return Boolean to represent a successful post
     */
    @Override
    public boolean addCustomer(Customer customer) {
        boolean success = false;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(insertCustomer);
            prep.setString(1, customer.getFirstName());
            prep.setString(2, customer.getLastName());
            prep.setString(3, customer.getCountry());
            prep.setString(4, customer.getPostalCode());
            prep.setString(5, customer.getPhone());
            prep.setString(6, customer.getEmail());
            int res = prep.executeUpdate();
            success = res != 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success;
    }

    /**
     * Method to update a Customer in the database
     * @param customer - Customer object to updated
     * @return Boolean to represent a successful update
     */
    @Override
    public boolean updateCustomer(Customer customer) {
        boolean success = false;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(updateCustomer);
            prep.setString(1, customer.getFirstName());
            prep.setString(2, customer.getLastName());
            prep.setString(3, customer.getCountry());
            prep.setString(4, customer.getPostalCode());
            prep.setString(5, customer.getPhone());
            prep.setString(6, customer.getEmail());
            prep.setString(7, customer.getId());
            int res = prep.executeUpdate();
            success = res != 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success;
    }

    /**
     * Method to get the number of customers per country
     * @return ArrayList of CustomerCountry objects
     */
    @Override
    public ArrayList<CustomerCountry> getNumberOfCustomersByCountry() {
        ArrayList<CustomerCountry> countries = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(countryCount);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                countries.add(
                        new CustomerCountry(
                                res.getString("Count"),
                                res.getString("Country")
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return countries;
    }

    /**
     * Method to get the highest spending customers
     * @return ArrarList of CustomerSpender objects sorted by spended amount
     */
    @Override
    public ArrayList<CustomerSpender> getSpenders() {
        ArrayList<CustomerSpender> spenders = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(highestSpenders);
            ResultSet res = prep.executeQuery();
            while(res.next()) {
                spenders.add(
                        new CustomerSpender(
                                new Customer(
                                        res.getString("CustomerId"),
                                        res.getString("FirstName"),
                                        res.getString("LastName"),
                                        res.getString("Country"),
                                        res.getString("PostalCode"),
                                        res.getString("Phone"),
                                        res.getString("Email")
                                ),
                                Double.parseDouble(res.getString("Total"))
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return spenders;
    }

    /**
     * Method to a customers favorite genre of music
     * @param id - ID of the specified Customer
     * @return CustomerGenre object
     */
    @Override
    public CustomerGenre getCustomerGenre(String id) {
        CustomerGenre genre = null;

        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            PreparedStatement prep = conn.prepareStatement(getMostPopularGenre);
            prep.setString(1, id);
            prep.setString(2, id);
            ResultSet res = prep.executeQuery();
            ArrayList<String> genres = new ArrayList<>();
            while(res.next()) {
                genres.add(
                        res.getString("Name")
                );
            }
            genre = new CustomerGenre(
                    id,
                    genres
            );
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return genre;
    }
}
