package com.assignment.Assignment2Java.repository;

import com.assignment.Assignment2Java.dataaccess.model.Track;

import java.sql.Connection;
import java.util.ArrayList;

public interface TrackRepository {
    public ArrayList<String> getRandomTracks();
    public ArrayList<String> getRandomArtists();
    public ArrayList<String> getRandomGenres();
    public ArrayList<Track> getTrackInfo(String track);
}
