package com.assignment.Assignment2Java.repository;

import com.assignment.Assignment2Java.dataaccess.model.Customer;
import com.assignment.Assignment2Java.dataaccess.model.CustomerCountry;
import com.assignment.Assignment2Java.dataaccess.model.CustomerGenre;
import com.assignment.Assignment2Java.dataaccess.model.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {

    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String id);
    public Customer getCustomerByName(String name);
    public ArrayList<Customer> getPageOfCustomers(String limit, String offset);
    public boolean addCustomer(Customer customer);
    public boolean updateCustomer(Customer customer);
    public ArrayList<CustomerCountry> getNumberOfCustomersByCountry();
    public ArrayList<CustomerSpender> getSpenders();
    public CustomerGenre getCustomerGenre(String id);
}
