# Assignment 6
## Access and expose a database

The purpose of this application is to create and expose a database to the web using Spring and Thymeleaf.

We have created a RESTful web API for customers, and a Thymeleaf web application for music tracks from the Chinook_Sqlite database.

The apllication is deployed on Heroku in a Docker container.\
Link to the application:\
https://spring-assignment-pf.herokuapp.com/


