FROM openjdk:16
ADD target/Assignment2Java-0.0.1-SNAPSHOT.jar assignment2.jar
ENTRYPOINT [ "java" , "-jar", "assignment2.jar"]